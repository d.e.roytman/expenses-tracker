//
//  PreviewContent.swift
//  Expenses Tracker
//
//  Created by Dmitry Roytman on 30/03/2022.
//

import Foundation

#if DEBUG

extension Transaction {
  static var preview: Transaction {
    Transaction(id: Int.random(in: 1 ... 1000000), date: "01/24/2022", institution: "Desjardins", account: "Visa Desjardins", merchant: "Apple", amount: 11.49, type: "debit", categoryId: 801, category: "Software", isPending: false, isTransfer: false, isExpense: true, isEdited: false)
  }
}

extension Array where Element == Transaction {
  static var preview: [Element] {
    return (0 ... 100).map { _ in .preview }
  }
}

var transactionPreviewData: Transaction = .preview


var transactionListPreviewData: [Transaction] = .preview

extension TransactionListViewModel {
  static var preview: TransactionListViewModel {
    let viewModel = TransactionListViewModel()
    viewModel.transactions = transactionListPreviewData
    return viewModel
  }
}
#endif
