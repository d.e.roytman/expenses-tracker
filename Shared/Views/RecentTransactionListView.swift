//
//  RecentTransactionListView.swift
//  Expenses Tracker (iOS)
//
//  Created by Dmitry Roytman on 30/03/2022.
//

import SwiftUI

struct RecentTransactionListView: View {
  @EnvironmentObject var transactionListViewModel: TransactionListViewModel
  var body: some View {
    VStack {
      HStack {
        Text("Recent transactions")
          .bold()
        Spacer()
        NavigationLink(destination: TransactionListView()) {
          HStack(spacing: 4) {
            Text("See All")
            Image(systemName: "chevron.right")
          }
          .foregroundColor(.text)
        }
      }
      .padding(8)
      
      VStack {
        ForEach(transactionListViewModel.transactions.prefix(6)) {
          TransactionRow(transaction: $0)
          if let last = transactionListViewModel.transactions[5], $0.id != last.id {
            Divider()
          }
        }
      }
    }
    .padding()
    .background(Color.systemBackground)
    .clipShape(
      RoundedRectangle(
        cornerRadius: 20,
        style: .continuous
      )
    )
    .shadow(color: .primary.opacity(0.2), radius: 10, x: .zero, y: 5)
  }
}

struct RecentTransactionListView_Previews: PreviewProvider {
  static var previews: some View {
    Group {
      NavigationView {
        RecentTransactionListView()
      }
      NavigationView {
        RecentTransactionListView()
          .preferredColorScheme(.dark)
      }
    }
    .environmentObject(TransactionListViewModel.preview)
  }
}
