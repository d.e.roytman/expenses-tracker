//
//  TransactionRow.swift
//  Expenses Tracker
//
//  Created by Dmitry Roytman on 30/03/2022.
//

import SwiftUI

struct TransactionRow: View {
  let transaction: Transaction
  var body: some View {
    HStack(spacing: 20) {
      TransactionRowIcon(icon: transaction.icon)
      TransactionRowContent(transaction: transaction)
      Spacer()
      TransactionRowSignedAmount(transaction: transaction)
    }
    .padding(.horizontal, 8)
  }
}

struct TransactionRow_Previews: PreviewProvider {
    static var previews: some View {
      Group {
        TransactionRow(transaction: Transaction.preview)
        TransactionRow(transaction: Transaction.preview)
          .preferredColorScheme(.dark)
      }
    }
}
