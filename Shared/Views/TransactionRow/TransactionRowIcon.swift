//
//  TransactionRowIcon.swift
//  Expenses Tracker (iOS)
//
//  Created by Dmitry Roytman on 30/03/2022.
//

import SwiftUI
import SwiftUIFontIcon

struct TransactionRowIcon: View {
  let icon: FontAwesomeCode
  
  var body: some View {
    RoundedRectangle(cornerRadius: 20)
      .foregroundColor(.icon.opacity(0.3))
      .frame(width: 44, height: 44)
      .overlay(
        FontIcon.text(.awesome5Solid(code: icon), fontsize: 24, color: .icon)
      )
  }
}

struct TransactionRowIcon_Previews: PreviewProvider {
  static var previews: some View {
    TransactionRowIcon(icon: Transaction.preview.icon)
  }
}
