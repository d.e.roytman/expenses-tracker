//
//  TransactionRowSignedAmount.swift
//  Expenses Tracker (iOS)
//
//  Created by Dmitry Roytman on 30/03/2022.
//

import SwiftUI

struct TransactionRowSignedAmount: View {
  let transaction: Transaction
  var body: some View {
    Text(transaction.signedAmount, format: .currency(code: "USD"))
      .font(.footnote)
      .bold()
      .lineLimit(1)
      .foregroundColor(
        transaction.transactionType == .credit ? .text : .primary
      )
  }
}

struct TransactionRowSignedAmount_Previews: PreviewProvider {
  static var previews: some View {
    TransactionRowSignedAmount(transaction: transactionPreviewData)
  }
}
