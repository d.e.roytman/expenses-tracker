//
//  TransactionRowContent.swift
//  Expenses Tracker (iOS)
//
//  Created by Dmitry Roytman on 30/03/2022.
//

import SwiftUI

struct TransactionRowContent: View {
  let transaction: Transaction
  var body: some View {
    VStack(alignment: .leading, spacing: 6) {
      Text(transaction.merchant)
        .font(.footnote)
        .bold()
        .lineLimit(1)
      Text(transaction.category)
        .font(.footnote)
        .opacity(0.7)
        .lineLimit(1)
      Text(transaction.dateParsed, format: .dateTime.day().month().year())
        .font(.footnote)
        .lineLimit(1)
        .foregroundColor(.secondary)
    }
  }
}

struct TransactionRowContent_Previews: PreviewProvider {
  static var previews: some View {
    TransactionRowContent(transaction: transactionPreviewData)
  }
}
