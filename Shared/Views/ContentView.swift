//
//  ContentView.swift
//  Shared
//
//  Created by Dmitry Roytman on 30/03/2022.
//

import SwiftUI
import SwiftUICharts

struct ContentView: View {
  @EnvironmentObject var transactionListViewModel: TransactionListViewModel

  var body: some View {
    NavigationView {
      ScrollView {
        VStack(alignment: .leading, spacing: 24) {
          Text("Expenses Tracker")
            .font(.title2)
            .bold()
          CardView {
            VStack {
              ChartLabel(transactionListViewModel.totalExpenses, type: .title, format: "$%.02f")
              LineChart()
            }
            .background(Color.systemBackground)
          }
          .data(transactionListViewModel.accumulateTransactions)
          .chartStyle(
            ChartStyle(
              backgroundColor: Color.systemBackground,
              foregroundColor: ColorGradient(Color.icon.opacity(0.4), Color.icon)
            )
          )
          .frame(height: 300)
          
          RecentTransactionListView()
        }
        .padding()
        .frame(maxWidth: .infinity)
      }
      .background(Color.background)
      .navigationBarTitleDisplayMode(.inline)
      .toolbar {
        // MARK: Notification icon
        ToolbarItem {
          Image(systemName: "bell.badge")
            .symbolRenderingMode(.palette)
            .foregroundStyle(Color.icon, .primary)
        }
      }
    }
    .navigationViewStyle(.stack)
    .accentColor(.primary)
    .task {
      await transactionListViewModel.fetchTransactions()
    }
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    Group {
      ContentView()
        .environmentObject(TransactionListViewModel.preview)
      ContentView()
        .environmentObject(TransactionListViewModel.preview)
        .preferredColorScheme(.dark)
    }
  }
}
