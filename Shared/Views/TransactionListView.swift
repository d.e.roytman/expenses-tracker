//
//  TransactionListView.swift
//  Expenses Tracker (iOS)
//
//  Created by Dmitry Roytman on 30/03/2022.
//

import SwiftUI

struct TransactionListView: View {
  @EnvironmentObject var transactionListViewModel: TransactionListViewModel
  
  var body: some View {
    VStack {
      List {
        ForEach(Array(transactionListViewModel.transactionsGroupByMonth), id: \.key) { month, transactions in
          Section {
            ForEach(transactions) { TransactionRow(transaction: $0) }
          } header: {
            Text(month)
          }
          .listSectionSeparator(.hidden)
        }
      }
      .listStyle(.plain)
    }
    .navigationTitle("Transactions")
    .navigationBarTitleDisplayMode(.inline)
  }
}

struct TransactionListView_Previews: PreviewProvider {
  static var previews: some View {
    Group {
      NavigationView {
        TransactionListView()
          .environmentObject(TransactionListViewModel.preview)
      }
      NavigationView {
        TransactionListView()
          .environmentObject(TransactionListViewModel.preview)
      }
      .preferredColorScheme(.dark)
    }
  }
}
