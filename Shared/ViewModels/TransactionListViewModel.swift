//
//  TransactionListViewModel.swift
//  Expenses Tracker (iOS)
//
//  Created by Dmitry Roytman on 30/03/2022.
//

import Foundation
import Collections

typealias TransactionGroup = OrderedDictionary<String, [Transaction]>
typealias TransactionPrefixSum = [(String, Double)]

final class TransactionListViewModel: ObservableObject {
  
  // MARK: - Internal properties
  
  @Published var transactions: [Transaction] = [] {
    didSet {
      guard !transactions.isEmpty else { return }
      transactionsGroupByMonth = TransactionGroup(grouping: transactions) { $0.month }
    }
  }
  @Published private(set) var error: String?
  
  @Published private(set) var transactionsGroupByMonth: TransactionGroup = [:] {
    didSet {
      accumulateTransactions = calculateAccumulatedTransactions()
    }
  }
  @Published private(set) var accumulateTransactions: TransactionPrefixSum = [] {
    didSet {
      let value = accumulateTransactions.last?.1 ?? .zero
      totalExpenses = value.formatted(.currency(code: "USD"))
    }
  }
  
  @Published private(set) var totalExpenses: String = ""

  // MARK: - Private properties
  
  private let session: URLSession
  private let decoder: JSONDecoder
  
  // MARK: - Init
  
  init(session: URLSession = .shared, decoder: JSONDecoder = .init()) {
    self.session = session
    self.decoder = decoder
  }
  
  // MARK: - Interanl methods
  
  @MainActor
  func fetchTransactions() async {
    do {
      let data = try await downloadData()
      let transactions = try decoder.decode([Transaction].self, from: data)
      self.transactions = transactions
    } catch {
      self.error = "An error has occured:\n\(error.localizedDescription)"
    }
  }
  
  // MARK: - Private methods
  
  private func downloadData() async throws -> Data {
    let (data, _) = try await session.data(from: .base)
    return data
  }
  
  private func calculateAccumulatedTransactions() -> TransactionPrefixSum {
    guard !transactions.isEmpty else { return [] }
    let today = "02/17/2022".toDate // Date()
    let dateInterval = Calendar.current.dateInterval(of: .month, for: today)!
    
    var sum: Double = .zero
    var cumulativeSum = TransactionPrefixSum()
    
    for date in stride(from: dateInterval.start, to: today, by: 60 * 60 * 24) {
      let dailyExpenses = transactions.filter { $0.dateParsed == date && $0.isExpense }
      let dailyTotal = dailyExpenses.reduce(0) { $0 - $1.signedAmount }
      sum += dailyTotal
      cumulativeSum.append((date.formatted(), sum))
    }
    
    return cumulativeSum
  }
}

// MARK: - helpers

extension URL {
  fileprivate static var base: URL {
    guard let url = URL(string: .base) else { fatalError("URL is expected to be created from path <\(String.base)>") }
    return url
  }
}

extension String {
  fileprivate static var base: String {
    "https://designcode.io/data/transactions.json"
  }
}
