//
//  DateFormatter.swift
//  Expenses Tracker
//
//  Created by Dmitry Roytman on 30/03/2022.
//

import Foundation

extension DateFormatter {
  static var allNumericUSA: DateFormatter {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "MM/dd/yyyy"
    return dateFormatter
  }
}
