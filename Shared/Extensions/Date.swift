//
//  Date.swift
//  Expenses Tracker (iOS)
//
//  Created by Dmitry Roytman on 30/03/2022.
//

import Foundation

extension Date: Strideable {
  var formatted: String {
    formatted(.dateTime.year().month().day())
  }
}
