//
//  String.swift
//  Expenses Tracker
//
//  Created by Dmitry Roytman on 30/03/2022.
//

import Foundation

extension String {
  var toDate: Date {
    guard let date = DateFormatter.allNumericUSA.date(from: self) else {
      assertionFailure("Date expected to be parsed from string <\(self)>")
      return Date()
    }
    return date
  }
}
