//
//  Color.swift
//  Expenses Tracker (iOS)
//
//  Created by Dmitry Roytman on 30/03/2022.
//

import SwiftUI

extension Color {
  static var background: Color { Color("Background") }
#if os(macOS)
  static var systemBackground: Color { Color(nsColor: .windowBackgroundColor) }
#else
  static var systemBackground: Color { Color(uiColor: .systemBackground) }
#endif
  static var icon: Color { Color("Icon") }
  static var text: Color { Color("Text") }
}
