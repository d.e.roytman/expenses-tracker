//
//  Transaction.swift
//  Expenses Tracker (iOS)
//
//  Created by Dmitry Roytman on 30/03/2022.
//

import Foundation
import SwiftUIFontIcon

struct Transaction: Identifiable, Decodable {
  let id: Int
  let date: String
  let institution: String
  let account: String
  var merchant: String
  let amount: Double
  let type: TransactionType.RawValue
  var categoryId: Int
  var category: String
  let isPending: Bool
  var isTransfer: Bool
  var isExpense: Bool
  var isEdited: Bool
  
  var dateParsed: Date {
    date.toDate
  }
  
  var month: String {
    dateParsed.formatted(.dateTime.year().month(.wide))
  }
  
  var signedAmount: Double {
    return transactionType == .credit ? amount : -amount
  }
  
  var transactionType: TransactionType {
    guard let transactionType = TransactionType(rawValue: type) else { fatalError() }
    return transactionType
  }
  
  var icon: FontAwesomeCode {
    guard let category = Category.all.first(where: { $0.id == categoryId }) else { return .question }
    return category.icon
  }
  
}

enum TransactionType: String {
  case debit = "debit"
  case credit = "credit"
}
