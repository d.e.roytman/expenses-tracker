//
//  Expenses_TrackerApp.swift
//  Shared
//
//  Created by Dmitry Roytman on 30/03/2022.
//

import SwiftUI

@main
struct Expenses_TrackerApp: App {
  @StateObject var transactionListViewModel = TransactionListViewModel()
  var body: some Scene {
    WindowGroup {
      ContentView()
        .environmentObject(transactionListViewModel)
    }
  }
}
